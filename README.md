# Stock Price Notifier App Frontend

[Test it here](https://sssf-frontend-f20e6.web.app/)

Welcome to the stock price notifier application. This application is powered by [finnhub.io](finnhub.io).
You can subscribe to symbols, see the price chart, and get notified when the price meet your condition.

Prerequiste

- [finnhub.io](finnhub.io) API key

### Installation

```
yarn
```

### Sign Up Page

Register with your information. Duplicate key and email are not permitted. You have to provide valid API key to register.

### Login Page

### Main Page

- search bar

Search for symbols and subscribe to it. Note that some symbols might not be available due to the limitation of API subsciption plan.

- symbol card

The symbol card shows company name, symbol, high, low, and their volumes. Click the "Timeseries" button will drop down the chart. The notication button will show the event status of the symbol and have 2 options

- to notify every hour
- to notify when the price meet the condition

### Who do I talk to?

- email me: weerapat.tec@hotmail.com
