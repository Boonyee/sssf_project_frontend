import React from "react";
import SymbolList from "./views/symbolList";
import { Helmet } from "react-helmet";

const App = () => {
  return (
    <>
      <Helmet>
        <title>Welcome.</title>
      </Helmet>
      <SymbolList />
    </>
  );
};

export default App;
