import React from 'react';
import { useHistory } from 'react-router-dom';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Grid from '@material-ui/core/Grid';

// async search bar
import AsyncSearch from './asyncSearch';

// apollo client
import { useApolloClient } from '@apollo/client';

const useStyles = makeStyles((theme) => {
  theme.shadows = [];
  return {
    bar: {
      backgroundColor: '#fff',
    },
    email: {
      color: 'black',
      float: 'right',
      position: 'relative',
      top: '50%',
      transform: 'translateY(-50%)',
      font: 'Montserrat',
      fontWeight: '80',
      letterSpacing: '1.5px',
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    },
    search: {
      display: 'inline-block',
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
      },
    },
    logoutButton: {
      color: 'black',
      float: 'right',
      position: 'relative',
      top: '50%',
      transform: 'translateY(-50%)',
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
  };
});

export default function PrimarySearchAppBar(prop) {
  const classes = useStyles();
  const client = useApolloClient();
  const history = useHistory();

  const logout = async () => {
    // setToken(null);
    if (window.confirm('Log out?')) {
      localStorage.clear();
      await client.clearStore();
      history.push('/');
    }
  };

  return (
    <div>
      <AppBar className={classes.bar} position="static">
        <Toolbar>
          <Grid container>
            <Grid item xs={2}></Grid>

            <Grid item xs={3}>
              <AsyncSearch subscribe={prop.subscribe} />
            </Grid>
            <Grid item xs={6}>
              <Typography className={classes.email} variant="h6">
                {prop.email}
              </Typography>
            </Grid>
            <Grid item xs={1}>
              <IconButton
                className={classes.logoutButton}
                color="inherit"
                onClick={logout}
              >
                <ExitToAppIcon />
              </IconButton>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
}
