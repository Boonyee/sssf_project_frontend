import fetch from 'cross-fetch';
import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';

import { symbolsQuery } from '../queries';

export default function Asynchronous({ subscribe }) {
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState([]);
  const [query, setQuery] = React.useState('');
  const [typing, setTyping] = React.useState(0);
  const loading = open && options.length === 0;
  const user_key = localStorage.getItem('user_key');
  const user_id = localStorage.getItem('user_id');

  React.useEffect(() => {
    let active = true;

    if (!loading) {
      return undefined;
    }
    console.log('useEffect');

    console.log('query.length :>> ', query.length);
    (async () => {
      try {
        if (query.length > 0) {
          const response = await fetch(
            `https://finnhub.io/api/v1/search?q=${query}&token=${user_key}`
          );
          const symbols = await response.json();

          if (active && !symbols.error) {
            setOptions(symbols.result);
          }
        }
      } catch (err) {
        console.log(err);
      }
    })();

    return () => {
      active = false;
    };
  }, [loading, query]);

  React.useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  const handleSearch = (e) => {
    // setQuery(e.target.value);
    if (typing) {
      clearTimeout(typing);
    }
    setTyping(
      setTimeout(() => {
        console.log('e.target.value :>> ', e.target.value);
        setQuery(e.target.value);
        setOptions([]);
      }, 500)
    );
  };

  const handleSelect = async (event, value) => {
    if (value) {
      //   alert(value.symbol);
      console.log('value:', value.symbol);

      //search for quote and ask for subscription
      const sub = window.confirm(
        `Do you want to subscribe to ${value.symbol}?`
      );
      if (sub) {
        subscribe({
          variables: {
            user_id,
            symbol: value.symbol,
          },
          awaitRefetchQueries: [
            {
              query: symbolsQuery,
              variables: { userId: user_id },
            },
          ],
        });
      }
    }
  };

  return (
    <Autocomplete
      id="asynchronous-demo"
      style={{ width: 300 }}
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
        setQuery('');
      }}
      getOptionSelected={(option, value) => option.symbol === value}
      getOptionLabel={(option) => option.symbol}
      options={options}
      loading={loading}
      onChange={handleSelect}
      renderInput={(params) => (
        <TextField
          onChange={handleSearch}
          {...params}
          label="Search by symbol..."
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
}
