export const register = (insertSubscription) => {
  if ('serviceWorker' in navigator) {
    const swUrl = `${window.location.origin.toString()}/serviceWorker.js`;
    navigator.serviceWorker
      .register(swUrl)
      .then(() => {
        console.log('Service Worker registered');
        return navigator.serviceWorker.ready;
      })
      .then((serviceWorkerRegistration) => {
        // console.log(
        //   "serviceWorkerRegistration :>> ",
        //   serviceWorkerRegistration
        // );
        getSubscription(serviceWorkerRegistration, insertSubscription);
        Notification.requestPermission();
      })
      .catch((err) => {
        throw err;
      });
  }
};

const getSubscription = (serviceWorkerRegistration, insertSubscription) => {
  serviceWorkerRegistration.pushManager
    .getSubscription()
    .then((subscription) => {
      // console.log('subscription(already have) :>> ', subscription);
      const user_id = localStorage.getItem('user_id');
      const token = localStorage.getItem('user_token');
      const subObject = localStorage.getItem('serviceWorkerRegistration');
      if (subscription && !subObject && user_id && token) {
        const sub = JSON.stringify(subscription);
        insertSubscription({
          variables: {
            userId: user_id,
            subscription: sub,
          },
        })
          .then((result) => {
            console.log('result', result);
          })
          .catch((err) => {
            throw err;
          });
        localStorage.setItem(
          'serviceWorkerRegistration',
          JSON.stringify({
            subscription,
          })
        );
      }
      if (!subscription && user_id && token) {
        const applicationServerKey = urlB64ToUint8Array(
          'BEaK63QfJtLg9GCr9EOCse0OPS_e52ipB6WxtieYaPyPUhlZw3nWqbpw0HKrXD9FOk18q_VjRGJJ0jrj3Cktp6Y'
        );
        serviceWorkerRegistration.pushManager
          .subscribe({
            userVisibleOnly: true,
            applicationServerKey,
          })
          .then((subscription) => {
            const sub = JSON.stringify(subscription);
            console.log('subscription(newly) :>> ', sub);

            insertSubscription({
              variables: {
                userId: user_id,
                subscription: sub,
              },
            })
              .then((result) => {
                console.log('subscription object collected', result);
              })
              .catch((err) => {
                throw err;
              });
            localStorage.setItem(
              'serviceWorkerRegistration',
              JSON.stringify({
                subscription,
              })
            );
          })
          .catch((err) => {
            throw err;
          });
      }
    })
    .catch((err) => {
      throw err;
    });
};

const urlB64ToUint8Array = (base64String) => {
  const padding = '='.repeat((4 - (base64String.length % 4)) % 4);
  const base64 = (base64String + padding).replace(/-/g, '+').replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
};
