// import ApolloClient from "apollo-boost";
// import { ApolloClient, InMemoryCache } from '@apollo/client';

// const apolloClient = new ApolloClient({
//   // uri: "https://stocks-app-101.herokuapp.com/v1/graphql",
//   uri: 'http://localhost:3000/graphql',
//   cache: new InMemoryCache(),
// });

import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

const httpLink = createHttpLink({
  uri: 'https://localhost:8000/graphql',
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem('user_token');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    },
  };
});

const apolloClient = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});

export default apolloClient;
