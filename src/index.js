import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider, useMutation } from '@apollo/client';
import App from './App';
import apolloClient from './apolloClient';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.scss';
import * as serviceWorker from './serviceWorker';
import { subscriptionMutation } from './queries';

// login and signup Page

import SignIn from './views/SignIn';
import SignUp from './views/SignUp';

// React Router

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

const StockPage = () => {
  const [insertSubscription] = useMutation(subscriptionMutation);
  useEffect(() => {
    serviceWorker.register(insertSubscription);
  }, []);
  return <App />;
};

ReactDOM.render(
  <ApolloProvider client={apolloClient}>
    <Router>
      <Switch>
        <Route exact path="/">
          <SignIn />
        </Route>
        <Route exact path="/signup">
          <SignUp />
        </Route>
        <Route exact path="/stocks">
          <StockPage />
        </Route>
      </Switch>
    </Router>
  </ApolloProvider>,
  document.getElementById('root')
);
