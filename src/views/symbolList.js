import React, { useState } from 'react';
import { useQuery, useMutation } from '@apollo/client';
import {
  Collapse,
  Card,
  CardBody,
  CardTitle,
  Badge,
  Button,
  Input,
  FormGroup,
  Label,
  UncontrolledAlert,
  Popover,
  PopoverHeader,
  PopoverBody,
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBell, faTimes } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames/bind';
import {
  symbolsQuery,
  eventsMutation,
  updateEventsMutation,
  subscribe as subscribeMutation,
  unsubscribe as unsubscribeMutation,
  clearEventMutation,
} from '../queries';
import Loader from '../components/loader';
import StockTimeseries from './stockTimeseries';
import ErrorBoundary from './errorBoundary';

// search bar
import PrimarySearchAppBar from '../components/searchBar';

// react router
import { useHistory } from 'react-router-dom';

const SymbolList = () => {
  const [expandedStockId, setExpandedStock] = useState('');
  // const [subscribeStatus, setsubscribeStatus] = useState(false);
  const [
    addEvent,
    { loading: eventMutationLoading, called, error: eventsMutationError },
  ] = useMutation(eventsMutation);
  const [
    updateEvent,
    { called: calledUpdateEventsMutation, error: updateEventsMutationError },
  ] = useMutation(updateEventsMutation);
  const [subscribe, subscribeResult] = useMutation(subscribeMutation, {
    onCompleted: (result) => {
      alert('subscription successful!');
      window.location.reload();
    },
    onError: (err) => {
      console.log(err);
      if (
        err.message ===
        'Found symbol in User but not in Symbol or already subscribed'
      ) {
        return alert('You already subscribed!');
      }
      alert('Sorry. This symbol is not available.');
    },
  });
  const [unsubscribe, unsubResult] = useMutation(unsubscribeMutation, {
    onCompleted: (result) => {
      window.location.reload();
    },
    onError: (err) => {
      console.log(err);
    },
  });

  const [clearEvent] = useMutation(clearEventMutation, {
    onError: (err) => {
      console.log(err);
    },
  });

  const history = useHistory();
  const registration = localStorage.getItem('serviceWorkerRegistration');
  const token = localStorage.getItem('user_token');
  const user_id = localStorage.getItem('user_id');
  const user_email = localStorage.getItem('user_email');
  if (!registration) {
    // console.log('registration :>> ', registration);
    //userId = JSON.parse(registration).userId;
  }
  if (!(token && user_id)) {
    console.log('no credentials found, heading back to login page');
    history.push('/');
  }
  // console.log('userId :>> ', userId);
  const { loading, error, data } = useQuery(symbolsQuery, {
    variables: { userId: user_id },
  });
  // console.log("loading :>> ", loading);
  // console.log("error :>> ", error);
  // console.log("data :>> ", data);

  const [triggerType, setTriggerType] = useState('');
  const [triggerValue, setTriggerValue] = useState(1);
  const [isSubscribePopoverOpen, handlePopoverToggle] = useState(null);

  if (loading) return <Loader />;
  if (error) {
    console.log(error);
    return <p>Something went wrong!</p>;
  }

  const isOpen = (stockId) => {
    return expandedStockId === stockId;
  };

  const subscribeToStock = async (stockId, eventId) => {
    handlePopoverToggle(null);
    console.log(eventId);
    if (!eventId) {
      console.log('adding event...');
      addEvent({
        variables: {
          user_id: user_id,
          symbol: stockId,
          triggerType,
          triggerValue: triggerType === 'time' ? 1 : Number(triggerValue),
        },
        refetchQueries: [
          {
            query: symbolsQuery,
            variables: { userId: user_id },
          },
        ],
      });
    } else {
      console.log('updating event...');
      updateEvent({
        variables: {
          id: eventId,
          triggerType,
          triggerValue: triggerType === 'time' ? 1 : Number(triggerValue),
        },
        refetchQueries: [
          {
            query: symbolsQuery,
            variables: { userId: user_id },
          },
        ],
      });
    }
  };

  const clearSubscribeEvent = async (id) => {
    handlePopoverToggle(null);
    await clearEvent({
      variables: { id },
      refetchQueries: [
        {
          query: symbolsQuery,
          variables: { userId: user_id },
        },
      ],
    });
  };

  const renderSubscribeOptions = (id, isSubscribed, symbolTriggerData) => {
    return (
      <PopoverBody>
        {isSubscribed ? (
          <div className="already-subscribed">
            You have already subscribed but you can go ahead and edit it!
          </div>
        ) : null}
        <FormGroup check>
          <Label check>
            <Input
              type="radio"
              name="trigger"
              value="time"
              onChange={(e) => setTriggerType(e.target.value)}
              checked={triggerType === 'time'}
            />
            Notify me every hour
          </Label>
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input
              type="radio"
              name="trigger"
              value="event"
              checked={triggerType === 'event'}
              onChange={(e) => setTriggerType(e.target.value)}
            />
            Notify me when the value of this stock is
            <Input
              type="text"
              disabled={!triggerType || triggerType === 'time'}
              onChange={(e) => setTriggerValue(e.target.value)}
              value={triggerValue}
            />
          </Label>
        </FormGroup>
        <Button
          className="submit-button"
          outline
          onClick={() => subscribeToStock(id, symbolTriggerData._id)}
          disabled={!triggerType}
        >
          Submit
        </Button>
        {isSubscribed ? (
          <Button
            className="submit-button"
            outline
            onClick={() => clearSubscribeEvent(symbolTriggerData._id)}
          >
            Clear
          </Button>
        ) : null}
      </PopoverBody>
    );
  };

  const setSubscribeValues = (id, symbolTriggerData) => {
    handlePopoverToggle(id);
    setTriggerType(symbolTriggerData.trigger_type);
    setTriggerValue(symbolTriggerData.trigger_value);
  };

  const renderSymbols = (data) => {
    // console.log('data :>> ', data.symbol);
    if (data.symbol.length === 0) {
      // console.log('no data return');
      return (
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100vh',
          }}
        >
          <h1>
            {' '}
            Looks like you haven't subscribed any stock yet :( <br />
            Try search some symbols!
          </h1>
        </div>
      );
    }
    return (
      <>
        {data.symbol.map((symbolData) => {
          const { id, company } = symbolData;
          const max = symbolData.stock_symbol_aggregate.max;
          const min = symbolData.stock_symbol_aggregate.min;
          const isSubscribed = symbolData.symbol_events.length !== 0;
          const symbolTriggerData = isSubscribed
            ? symbolData.symbol_events[0]
            : {
                trigger_type: '',
                trigger_value: 0,
              };

          return (
            <div key={id}>
              <div className="card-container">
                <Card>
                  <CardBody>
                    <CardTitle className="card-title">
                      <span className="company-name">{company} </span>
                      <Badge color="dark" pill>
                        {id}
                      </Badge>
                      <div
                        className={classNames({
                          bell: true,
                          disabled: isSubscribed,
                        })}
                        id={`subscribePopover-${id}`}
                      >
                        <FontAwesomeIcon icon={faBell} title="Subscribe" />
                      </div>
                    </CardTitle>
                    <div className="metrics">
                      <div className="metrics-row">
                        <span className="metrics-row--label">High:</span>
                        <span className="metrics-row--value">{max.high}</span>
                        <span className="metrics-row--label"> (Volume: </span>
                        <span className="metrics-row--value">{max.volume}</span>
                        )
                      </div>
                      <div className="metrics-row">
                        <span className="metrics-row--label">Low: </span>
                        <span className="metrics-row--value">{min.low}</span>
                        <span className="metrics-row--label"> (Volume: </span>
                        <span className="metrics-row--value">{min.volume}</span>
                        )
                      </div>
                    </div>
                    <Button
                      className="timeseries-btn"
                      outline
                      onClick={() => toggleTimeseries(id)}
                    >
                      Timeseries
                    </Button>{' '}
                    <Button
                      className="timeseries-btn"
                      outline
                      onClick={() => {
                        if (
                          window.confirm(
                            `Are you sure you want to unsubscribe ${id}?`
                          )
                        ) {
                          unsubscribe({
                            variables: {
                              user_id,
                              symbol: id,
                            },
                            awaitRefetchQueries: [
                              {
                                query: symbolsQuery,
                                variables: { userId: user_id },
                              },
                            ],
                          });
                        }
                      }}
                    >
                      Unsubscribe
                    </Button>{' '}
                  </CardBody>
                </Card>
                <Popover
                  className="popover-custom"
                  placement="bottom"
                  target={`subscribePopover-${id}`}
                  isOpen={isSubscribePopoverOpen === id}
                  toggle={() => setSubscribeValues(id, symbolTriggerData)}
                >
                  <PopoverHeader>
                    Notification Options
                    <span className="popover-close">
                      <FontAwesomeIcon
                        icon={faTimes}
                        onClick={() => handlePopoverToggle(null)}
                      />
                    </span>
                  </PopoverHeader>
                  {renderSubscribeOptions(id, isSubscribed, symbolTriggerData)}
                </Popover>
              </div>
              <Collapse isOpen={expandedStockId === id}>
                {isOpen(id) ? <StockTimeseries symbol={id} /> : null}
              </Collapse>
            </div>
          );
        })}
        {eventMutationLoading ? (
          <UncontrolledAlert color="info">
            Subscribing to stock...
          </UncontrolledAlert>
        ) : null}
        {(called && !eventsMutationError) || calledUpdateEventsMutation ? (
          <UncontrolledAlert color="success">
            Subscription successful!
          </UncontrolledAlert>
        ) : null}
        {eventsMutationError || updateEventsMutationError ? (
          <UncontrolledAlert color="danger">
            Error while subscribing!
          </UncontrolledAlert>
        ) : null}
      </>
    );
  };

  const toggleTimeseries = (id) => {
    id === expandedStockId ? setExpandedStock(null) : setExpandedStock(id);
  };

  return (
    <ErrorBoundary>
      <PrimarySearchAppBar email={user_email} subscribe={subscribe} />
      {renderSymbols(data)}
    </ErrorBoundary>
  );
};

export default SymbolList;
