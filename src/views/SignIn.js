import React, { useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

// React Router
import { useHistory } from 'react-router-dom';
import { Helmet } from 'react-helmet';

// Apollo graphql
import { useMutation } from '@apollo/client';
import { login as loginMutation } from '../queries';

// function Copyright() {
//   return (
//     <Typography variant="body2" color="textSecondary" align="center">
//       {'Copyright © '}
//       <Link color="inherit" href="https://material-ui.com/">
//         Your Website
//       </Link>{' '}
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const history = useHistory();
  const classes = useStyles();

  const [login, result] = useMutation(loginMutation, {
    onError: (error) => {
      // setError(error.graphQLErrors[0].message);
      // alert(error);
      if (error.graphQLErrors[0].extensions.code === 'UNAUTHENTICATED') {
        setError('Incorrect email or password.');
      }
      // console.log(error.graphQLErrors);
    },
  });

  useEffect(() => {
    let token = localStorage.getItem('user_token');
    let user_id = localStorage.getItem('user_id');
    let user_email = localStorage.getItem('user_email');
    let user_key = localStorage.getItem('user_key');
    if (token && user_id && user_email && user_key) {
      console.log('found user, proceeding to main page');
      history.push('/stocks');
    } else if (result.data) {
      token = result.data.login.token;
      user_id = result.data.login.user._id;
      user_email = result.data.login.user.email;
      user_key = result.data.login.user.key;
      // setToken(token);
      localStorage.setItem('user_token', token);
      localStorage.setItem('user_id', user_id);
      localStorage.setItem('user_email', user_email);
      localStorage.setItem('user_key', user_key);
      history.push('/stocks');
    }
  }, [history, result.data]);

  // const handleSubmit = (e) => {
  //   e.preventDefault();
  //   history.push('/stocks');
  // };

  const submit = async (event) => {
    event.preventDefault();

    login({ variables: { email, password } });
  };

  const handleSignup = (event) => {
    event.preventDefault();

    history.push('/signup');
  };

  return (
    <>
      <Helmet>
        <title>Log in</title>
      </Helmet>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          {error !== '' && <Typography component="h2">{error}</Typography>}
          <form className={classes.form} onSubmit={submit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email"
              name="email"
              autoComplete="email"
              autoFocus
              value={email}
              onChange={({ target }) => setEmail(target.value)}
              error={Boolean(error)}
              helperText={error}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              value={password}
              onChange={({ target }) => setPassword(target.value)}
              error={Boolean(error)}
            />
            {/* <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            /> */}

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
            </Button>

            <Grid container>
              <Grid item xs>
                {/* <Link href="#" variant="body2">
                  Forgot password?
                </Link> */}
              </Grid>
              <Grid item>
                <Link href="#" variant="body2" onClick={handleSignup}>
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={8}>{/* <Copyright /> */}</Box>
      </Container>
    </>
  );
}
