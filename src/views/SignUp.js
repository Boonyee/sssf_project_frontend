import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Autocomplete from '@material-ui/lab/Autocomplete';

import { useHistory } from 'react-router-dom';
import { signup as signupMutation } from '../queries';
import { useMutation } from '@apollo/client';

// function Copyright() {
//   return (
//     <Typography variant="body2" color="textSecondary" align="center">
//       {'Copyright © '}
//       <Link color="inherit" href="https://material-ui.com/">
//         Your Website
//       </Link>{' '}
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const jobs = ['Investor', 'Software Developer', 'Educator', 'Student', 'Other'];

export default function SignUp() {
  const classes = useStyles();
  const history = useHistory();
  const [form, setForm] = useState({
    email: '',
    password: '',
    organization: '',
    job: '',
    key: '',
  });
  const [error, setError] = useState('');

  const [signup, result] = useMutation(signupMutation, {
    onCompleted: ({ signup }) => {
      localStorage.setItem('user_token', signup.token);
      localStorage.setItem('user_id', signup.user._id);
      localStorage.setItem('user_key', signup.user.key);
      localStorage.setItem('user_email', signup.user.email);
      history.push('/stocks');
    },
    onError: (err) => {
      console.log(err);
      if (err.message === 'User validation failed: email: invalid email') {
        setError('Invalid email');
        // console.log(error);
      }
      if (err.message === 'User validation failed: key: invalid key') {
        setError('Invalid key');
        // console.log(error);
      }
      if (err.message === 'User or API token already exist') {
        setError('User or API token already exist');
      }
    },
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    form.job = form.job.replaceAll(' ', '_');
    signup({ variables: form });
  };

  const handleLogin = (event) => {
    event.preventDefault();
    history.push('/');
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                type="email"
                error={
                  error === 'Invalid email' ||
                  error === 'User or API token already exist'
                    ? true
                    : false
                }
                helperText={
                  (error === 'Invalid email' ||
                    error === 'User or API token already exist') &&
                  error
                }
                onChange={({ target }) =>
                  setForm({
                    ...form,
                    email: target.value,
                  })
                }
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={({ target }) =>
                  setForm({
                    ...form,
                    password: target.value,
                  })
                }
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="key"
                label="API Key"
                id="key"
                error={
                  error === 'Invalid key' ||
                  error === 'User or API token already exist'
                    ? true
                    : false
                }
                helperText={
                  (error === 'Invalid key' ||
                    error === 'User or API token already exist') &&
                  error
                }
                onChange={({ target }) =>
                  setForm({
                    ...form,
                    key: target.value,
                  })
                }
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="organization"
                label="Organization"
                id="organization"
                onChange={({ target }) =>
                  setForm({
                    ...form,
                    organization: target.value,
                  })
                }
              />
            </Grid>
            <Grid item xs={12}>
              <Autocomplete
                onChange={(event, value) =>
                  setForm({
                    ...form,
                    job: value,
                  })
                }
                options={jobs}
                fullWidth
                name="job"
                id="job"
                renderInput={(params) => (
                  <TextField {...params} label="Job" variant="outlined" />
                )}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="#" variant="body2" onClick={handleLogin}>
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>{/* <Copyright /> */}</Box>
    </Container>
  );
}
