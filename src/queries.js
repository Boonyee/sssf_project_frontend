import { gql } from '@apollo/client';

export const symbolsQuery = gql`
  query getSymbols($userId: String) {
    symbol(user_id: $userId) {
      id
      company
      symbol_events(user_id: $userId) {
        _id
        symbol
        trigger_type
        trigger_value
      }
      stock_symbol_aggregate {
        max {
          high
          volume
        }
        min {
          low
          volume
        }
      }
    }
  }
`;

export const stocksDataQuery = gql`
  query getStocksData($symbol: String) {
    stock_data(
      order_by: { time: desc }
      where: { symbol: $symbol }
      limit: 25
    ) {
      high
      low
      open
      close
      volume
      time
    }
  }
`;

export const subscriptionListQuery = gql`
  query getSubscriptions($user_id: String) {
    events(filters: { user_id: $user_id }) {
      symbol
    }
  }
`;

export const subscriptionMutation = gql`
  mutation userSubscription($userId: String, $subscription: String) {
    insert_subscription(object: { id: $userId, subscription: $subscription }) {
      _id
    }
  }
`;

export const eventsMutation = gql`
  mutation addEvent(
    $symbol: String
    $user_id: String
    $triggerType: String
    $triggerValue: Float
  ) {
    insert_events(
      objects: {
        symbol: $symbol
        user_id: $user_id
        trigger_type: $triggerType
        trigger_value: $triggerValue
      }
    ) {
      _id
    }
  }
`;

export const updateEventsMutation = gql`
  mutation updateEvents($id: ID, $triggerType: String, $triggerValue: Float) {
    update_events(
      id: $id
      set: { trigger_type: $triggerType, trigger_value: $triggerValue }
    ) {
      _id
      trigger_type
      trigger_value
    }
  }
`;

export const login = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
      user {
        _id
        email
        key
      }
    }
  }
`;

export const signup = gql`
  mutation signup(
    $email: EmailAddress!
    $password: String!
    $job: JobEnum!
    $organization: String!
    $key: String!
  ) {
    signup(
      object: {
        email: $email
        password: $password
        job: $job
        organization: $organization
        key: $key
      }
    ) {
      token
      user {
        _id
        email
        key
      }
    }
  }
`;

export const subscribe = gql`
  mutation subscribe($user_id: String!, $symbol: String!) {
    subscribe_stock(user_id: $user_id, symbol: $symbol) {
      _id
    }
  }
`;

export const unsubscribe = gql`
  mutation unsubscribe($user_id: String!, $symbol: String!) {
    unsubscribe_stock(user_id: $user_id, symbol: $symbol) {
      _id
    }
  }
`;

export const clearEventMutation = gql`
  mutation clearEvent($id: ID!) {
    delete_events(id: $id) {
      _id
    }
  }
`;
